package visitorversion;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExplodingAsteroidVisitorTest {
    private ExplodingAsteroidVisitor explodingAsteroidVisitor;
    private Spaceship spaceship;
    private ApolloSpacecraft apolloSpacecraft;

    @Before
    public void setUp() {
        explodingAsteroidVisitor = new ExplodingAsteroidVisitor();

        spaceship = new Spaceship();
        apolloSpacecraft = new ApolloSpacecraft();
    }

    @Test
    public void spaceshipHittedAndDestroy() throws Exception {
        spaceship.accept(explodingAsteroidVisitor);

        assertTrue("ExplodingAsteroid (visitor) hit a Spaceship (visitable)", spaceship.isHitted());
        assertTrue("ExplodingAsteroid (visitor) destroy a Spaceship (visitable)", spaceship.isDestroy());
    }

    @Test
    public void apolloSpacecraftHittedAndDestroy() throws Exception {
        apolloSpacecraft.accept(explodingAsteroidVisitor);

        assertTrue("ExplodingAsteroid (visitor) hit an ApolloSpacecraft (visitable)", apolloSpacecraft.isHitted());
        assertTrue("ExplodingAsteroid (visitor) destroy an ApolloSpacecraft (visitable)", apolloSpacecraft.isDestroy());
    }
}
