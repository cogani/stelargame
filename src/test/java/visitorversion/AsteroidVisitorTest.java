package visitorversion;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AsteroidVisitorTest {

    private AsteroidVisitor asteroidVisitor;
    private Spaceship spaceship;
    private ApolloSpacecraft apolloSpacecraft;

    @Before
    public void setUp() {
        asteroidVisitor = new AsteroidVisitor();

        spaceship = new Spaceship();
        apolloSpacecraft = new ApolloSpacecraft();
    }

    @Test
    public void spaceshipHittedAndNotDestroy() throws Exception {
        spaceship.accept(asteroidVisitor);

        //assertTrue("Asteroid (visitor) hit a Spaceship (visitable)", asteroidVisitor.);
        assertTrue("Asteroid (visitor) hit a Spaceship (visitable)", spaceship.isHitted());
        assertFalse("Asteroid (visitor) hit a Spaceship (visitable)", spaceship.isDestroy());
    }

    @Test
    public void apolloSpacecraftHittedAndNotDestroy() throws Exception {
        apolloSpacecraft.accept(asteroidVisitor);

        assertTrue("Asteroid (visitor) hit a ApolloSpacecraft (visitable)",apolloSpacecraft.isHitted());
        assertFalse("Asteroid (visitor) hit a ApolloSpacecraft (visitable)",apolloSpacecraft.isDestroy());
    }
}
