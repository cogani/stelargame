package noconditionaltry;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;


public class AllKindAsteroidTest {

    private Asteroid asteroid;
    private ExplodingAsteroid explodingAsteroid;
    private SpaceShip spaceShip;
    private SpaceShip apolloSpacecraft;

    @Before
    public void setUp() {
        asteroid = new Asteroid();
        explodingAsteroid = new ExplodingAsteroid();

        spaceShip = new SpaceShip();
        apolloSpacecraft = new ApolloSpacecraft();
    }

    @Test
    public void asteroidCollideWithSpaceShipHitSpaceShit() {
          assertEquals("Asteroid operation a Spaceship",asteroid.collideWith(spaceShip));
    }

    @Test
    public void asteroidCollideWithApolloSpacecraftHitApolloSpacecraft() {
        assertEquals("Asteroid operation an ApolloSpacecraft",asteroid.collideWith(apolloSpacecraft));
    }

    @Test
    public void explodingAsteroidCollideWithSpaceShipHitSpaceShit() {
        assertEquals("ExplodingAsteroid operation a Spaceship",explodingAsteroid.collideWith(spaceShip));
    }

    @Test
    public void explodingAsteroidCollideWithApolloSpacecraftHitApolloSpacecraft() {
        assertEquals("ExplodingAsteroid operation an ApolloSpacecraft",explodingAsteroid.collideWith(apolloSpacecraft));
    }


}
