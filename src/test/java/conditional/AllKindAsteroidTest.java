package conditional;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class AllKindAsteroidTest {

    private Asteroid asteroid;
    private Asteroid explodingAsteroid;
    private Spaceship spaceship;
    private Spaceship apolloSpacecraft;

    @Before
    public void setUp() {
        asteroid = new Asteroid();
        explodingAsteroid = new ExplodingAsteroid();

        spaceship = new Spaceship();
        apolloSpacecraft = new ApolloSpacecraft();
    }

    @Test
    public void asteroidCollideWithSpaceshipHitSpaceship() {
          assertEquals("Asteroid operation a Spaceship",asteroid.collideWith(spaceship));
    }

    @Test
    public void asteroidCollideWithApolloSpacecraftHitApolloSpacecraft() {
        assertEquals("Asteroid operation an ApolloSpacecraft",asteroid.collideWith(apolloSpacecraft));
    }

    @Test
    public void explodingAsteroidCollideWithSpaceshipHitSpaceship() {
        assertEquals("ExplodingAsteroid operation a Spaceship",explodingAsteroid.collideWith(spaceship));
    }

    @Test
    public void explodingAsteroidCollideWithApolloSpacecraftHitApolloSpacecraft() {
        assertEquals("ExplodingAsteroid operation an ApolloSpacecraft",explodingAsteroid.collideWith(apolloSpacecraft));
    }
}
