package conditional;

public class ExplodingAsteroid extends Asteroid {
    @Override
    public String collideWith(Spaceship spaceship) {
        String className = spaceship.getClass().getSimpleName();
        String result;

        if (className.equals("Spaceship")) {
            result = "ExplodingAsteroid operation a Spaceship";
        } else {
            result = "ExplodingAsteroid operation an ApolloSpacecraft";
        }

        return result;
    }
}

