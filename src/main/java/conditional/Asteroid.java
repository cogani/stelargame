package conditional;

public class Asteroid {
    public String collideWith(Spaceship spaceship) {

        String className = spaceship.getClass().getSimpleName();
        String result;

        if (className.equals("Spaceship")) {
            result = "Asteroid operation a Spaceship";
        } else {
            result = "Asteroid operation an ApolloSpacecraft";
        }

        return result;
    }

}
