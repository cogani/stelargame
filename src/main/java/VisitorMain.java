import visitorversion.*;

public final class VisitorMain {
    //Utility classes should not have a public or default constructor
    private VisitorMain() {
    }

    public static void main(String args[]) {
        AsteroidVisitor asteroidVisitor = new AsteroidVisitor();
        ExplodingAsteroidVisitor explodingAsteroidVisitor = new ExplodingAsteroidVisitor();

        testingWithArray(asteroidVisitor, explodingAsteroidVisitor);
    }

    private static void testingWithArray(AsteroidVisitor asteroidVisitor, ExplodingAsteroidVisitor explodingAsteroidVisitor) {
        Spaceship spaceship = new Spaceship();
        ApolloSpacecraft apolloSpacecraft = new ApolloSpacecraft();

        // Probando con array de visitadores
        Visitor[] visitors = {asteroidVisitor, explodingAsteroidVisitor};
        for (Visitor visitor : visitors) {
            // Hitting to spaceship
            spaceship.accept(visitor);
            showResult(visitor, spaceship);
            // Hitting to apolloSpacecraft
            apolloSpacecraft.accept(visitor);
            showResult(visitor, apolloSpacecraft);
        }
    }

    public static void showResult(Visitor visitor, Target target) {
        String source = visitor.getClass().getSimpleName();
        System.out.println(source+" hitting to "+target.getType() + ": hitted-> " + target.isHitted() + ", destroy-> " + target.isDestroy());
    }
}
