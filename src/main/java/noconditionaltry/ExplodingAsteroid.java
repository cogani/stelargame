package noconditionaltry;

public class ExplodingAsteroid extends Asteroid {
    @Override
    public String collideWith(SpaceShip spaceShip) {
        String result = "ExplodingAsteroid operation a Spaceship";

        return result;

    }

    @Override
    public String collideWith(ApolloSpacecraft apolloSpacecraft) {
        String result = "ExplodingAsteroid operation an ApolloSpacecraft";

        return result;
    }
}
