package noconditionaltry;

public class Asteroid {
    public String collideWith(SpaceShip spaceShip) {
        String result = "Asteroid operation a Spaceship";

        return result;
    }

    public String collideWith(ApolloSpacecraft apolloSpacecraft) {
        String result = "Asteroid operation an ApolloSpacecraft";


        return result;
    }
}
