import conditional.ApolloSpacecraft;
import conditional.Asteroid;
import conditional.ExplodingAsteroid;
import conditional.Spaceship;

//La clase ConditionalMain debería declararse final.
final class ConditionalMain {
    //Utility classes should not have a public or default constructor
    private ConditionalMain(){};

    public static void main(String args[]){
        Asteroid asteroid = new Asteroid();
        Asteroid explodingAsteroid = new ExplodingAsteroid();

        Spaceship spaceship = new Spaceship();
        Spaceship apolloSpacecraft = new ApolloSpacecraft();

        System.out.println(asteroid.collideWith(spaceship));
        System.out.println(asteroid.collideWith(apolloSpacecraft));

        System.out.println(explodingAsteroid.collideWith(spaceship));
        System.out.println(explodingAsteroid.collideWith(apolloSpacecraft));
    }
}
