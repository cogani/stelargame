package visitorversion;

public interface VisitableElement {
    void accept(Visitor visitor);
}
