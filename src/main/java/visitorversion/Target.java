package visitorversion;

public interface Target {
    String getType();
    void hit();
    void destroy();
    boolean isHitted();
    boolean isDestroy();
}
