package visitorversion;

public class ApolloSpacecraft implements VisitableElement, Target {
    private final String whoAmI = "an ApolloSpacecraft (visitable)";

    private boolean hitted;
    private boolean destroy;

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getType() {
        return whoAmI;
    }

    public void hit() {
        hitted = true;
    }

    public void destroy() {
        destroy = true;
    }

    public boolean isHitted() {
        return hitted;
    }

    public boolean isDestroy() {
        return destroy;
    }
}
