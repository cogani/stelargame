package visitorversion;

public class AsteroidVisitor implements Visitor{
    private String nameVisitor = "Asteroid (visitor)";

    private String visitResult;

    @Override
    public void visit(Spaceship spaceshipElement) {
        generateVisitTo(spaceshipElement);
    }

    @Override
    public void visit(ApolloSpacecraft apolloSpacecraftElement) {
        generateVisitTo(apolloSpacecraftElement);

    }

    private void generateVisitTo(Target targetElement) {
        targetElement.hit();
    }
}
