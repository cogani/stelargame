package visitorversion;

public class ExplodingAsteroidVisitor implements Visitor{
    private String nameVisitor = "ExplodingAsteroid (visitor)";

    @Override
    public void visit(Spaceship spaceshipElement) {
        generateVisitTo(spaceshipElement);
        spaceshipElement.destroy();
    }

    @Override
    public void visit(ApolloSpacecraft apolloSpacecraftElement) {
        generateVisitTo(apolloSpacecraftElement);
        apolloSpacecraftElement.destroy();
    }

    private void generateVisitTo(Target targetElement) {
        targetElement.hit();
        targetElement.destroy();
    }
}
