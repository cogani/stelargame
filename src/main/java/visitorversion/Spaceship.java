package visitorversion;

public class Spaceship implements VisitableElement, Target {
    private final String type= "a Spaceship (visitable)";
    private boolean hitted;
    private boolean destroy;

    private String whoAmI = "a Spaceship (visitable)";

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void hit() {
        hitted = true;
    }

    public void destroy() {
        destroy = true;
    }

    public boolean isHitted() {
        return hitted;
    }

    public boolean isDestroy() {
        return destroy;
    }
}
