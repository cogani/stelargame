package visitorversion;

public interface Visitor {
    void visit(Spaceship spaceshipElement);
    void visit(ApolloSpacecraft apolloSpacecraftElement);
}
