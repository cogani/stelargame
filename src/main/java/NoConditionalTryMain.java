import noconditionaltry.ApolloSpacecraft;
import noconditionaltry.Asteroid;
import noconditionaltry.ExplodingAsteroid;
import noconditionaltry.SpaceShip;

public final class NoConditionalTryMain {
    //Utility classes should not have a public or default constructor
    private NoConditionalTryMain(){};

    public static void main(String args[]){
        Asteroid asteroid = new Asteroid();
        Asteroid explodingAsteroid = new ExplodingAsteroid();

        SpaceShip spaceShip = new SpaceShip();
        SpaceShip apolloSpacecraft = new ApolloSpacecraft();

        System.out.println(asteroid.collideWith(spaceShip));
        System.out.println(asteroid.collideWith(apolloSpacecraft));

        System.out.println(explodingAsteroid.collideWith(spaceShip));
        System.out.println(explodingAsteroid.collideWith(apolloSpacecraft));
    }
}
